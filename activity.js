// s31 Activity- Node.js


// 1. What directive is used by Node.js in loading the modules it needs?
// Answer:
// "require" directive - used to load Node.js modules


// 2. What Node.js module contains a method for server creation?
// Answer:
// "http module" - lets the Node.js transfer data using HTTP
// The http module contains the function to create the server


// 3. What is the method of the http object responsible for creating a server using Node.js?
// Answer:
// "createServer()" method - used to create HTTP server that listens to requests in a specified port and gives responses back to the client


// 4. What method of the response object allows us to set status codes and content types?
// Answer:
// "writeHead()" method - used to set a status code for the response and set the content type of the response


// 5. Where will console.log() output its contents when run in Node.js?
// Answer:
// The console.log() outputs a message to the terminal


// 6. What property of the request object contains the address's endpoint?
// Answer:
// request.url








