// s31 Activity- Node.js

let http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {

	if( request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('This is the login page!');
	} else {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Error!');
	}

});

server.listen(port);

console.log(`successfully running: ${port}`);